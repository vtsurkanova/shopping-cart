import 'bootstrap/dist/css/bootstrap-grid.min.css';
import $ from 'jquery';
import '@fortawesome/fontawesome-free/js/all.min';
import './scss/style.scss';

global.jQuery = $;
global.$ = $;

$(document).ready(function($){
    $('.pi-desc__cart-btn').on("click", function(e){
        e.preventDefault();
        $('.modal-block').fadeIn( "slow", function() {
        });
        $('.modal-inner').css("transform","translateX(0)");
    });
    $('.modal-inner__title').on("click", function(e){
        e.preventDefault();
        $('.modal-inner').css("transform","translateX(2000px)");
        $('.modal-block').fadeOut( "slow", function() {
        });
    });
    $(document).on("click", function(event){
        if ($(event.target).attr('class') == $(".modal-block").attr('class')) {
            $('.modal-inner').css("transform","translateX(2000px)");
            $(".modal-block").fadeOut( "slow", function() {});
        }
    });

    //for 50items
    $('.pi-desc__quantity').on("click", function(e){
        e.preventDefault();
        let item = $(e.target).next("ol")[0];
        $(item).fadeIn( "slow", function() {
            $(item).css("height","180px");
        });
    });

    $('.quantity-list').on("click", function(e){
        $('.quantity-list').css("height","0px");
    });
});